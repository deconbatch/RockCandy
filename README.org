* Rock Candy.
One of creative coding works by deconbatch.
[[https://deconbatch.blogspot.com/2017/11/rock-candy.html]['Rock Candy.' on Creative Coding : Examples with Code.]]
** Description
I was interested in [[http://deconbatch.blogspot.com/2017/11/full-bloom.html]['Full Bloom']] background.
It creates a vortex with simple rectangles.

I tried to enhance contrast with this code
:  float fillBri = constrain(map(noise(noiseBri), 0.0, 1.0, -_baseBri * 0.5, _baseBri), 0.0, 100.0);

This code does not display any images on screen but generates image files in frames directory. 
** Change log
   - created 2017.11.13
   - updated 2019.08.30
     - contrast enhancement.
     - add casing.
     - refactoring done.
