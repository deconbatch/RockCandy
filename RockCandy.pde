/**
 * Rock Candy.
 * 
 * @author @deconbatch
 * @version 0.2
 * Processing 3.2.1
 * draw circle with rect() in multiple blend mode.
 * ver 0.1 : created 2017.11.13
 * ver 0.2 : updated 2019.08.30
 *           contrast enhancement.
 *           add casing.
 *           refactoring done.
 */

void setup() {
  size(980, 980);
  colorMode(HSB, 360.0, 100.0, 100.0, 100.0);
  noStroke();
  smooth();
  noLoop();
}

void draw() {

  float baseHue = random(360.0);

  background(0.0, 0.0, 100.0, 100.0);
  translate(width * 0.5, height * 0.5);

  blendMode(BLEND);
  drawVortex(baseHue, 200.0, 20.0);
  blendMode(ADD);
  drawVortex(baseHue + 60.0, 10.0, 0.5);
  blendMode(BLEND);
  rectMode(CENTER);
  casing();

  saveFrame("frames/####.png");
  exit();

}

/**
 * drawVortex easing function.
 * @param  _baseHue    : hue value of rectangle.
 * @param  _baseBri    : brightness value of rectangle.
 * @param  _baseWidth  : base width of rectangle.
 */
void drawVortex(float _baseHue, float _baseBri, float _baseWidth) {

  int   drawMax = 3;
  float rectY   = random(height);
  float rectH   = height;
  
  float noiseSat = random(100.0);
  float noiseBri = random(100.0);
  float noiseRot = random(100.0);

  for (int drawCnt = 1; drawCnt <= drawMax; ++drawCnt) {

    float rectW   = map(drawCnt, 1, drawMax, 1.0, _baseWidth);
    float fillHue = _baseHue + map(drawCnt, 1, drawMax, 0.0, 90.0);
    float fillAlp = map(drawCnt, 1, drawMax, 60.0, 5.0);

    pushMatrix();
    for (float rectX = -width * 0.5; rectX < width * 0.5; rectX += 0.05) {

      rotate(PI * noise(noiseRot) * 0.002);

      float fillSat = map(noise(noiseSat), 0.0, 1.0, 80.0, 100.0);
      float fillBri = constrain(map(noise(noiseBri), 0.0, 1.0, -_baseBri * 0.5, _baseBri), 0.0, 100.0); // amplify contrast
      fill(fillHue % 360.0, fillSat, fillBri, fillAlp);
      rect(rectX, -rectY, rectW, rectH);

      noiseSat += 0.002;
      noiseBri += 0.003;
      noiseRot += 0.005;
    
    }
    popMatrix();

  }
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(58.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}
